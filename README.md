# Webdevops Debian 9 PHP 5.5.38 image

[![pipeline status](https://gitlab.com/webdevops/debian-9-php55/badges/master/pipeline.svg)](https://gitlab.com/webdevops/debian-9-php55/commits/master)

This is a special legacy image to run incompatible old legacy software.

It's based on https://github.com/webdevops/Dockerfile but uses the Debian-9 (stretch) base-app
image in combination with the https://www.liveconfig.com/wiki/de/multiphp repository to install
PHP 5.5.38.
