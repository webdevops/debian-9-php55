#!/usr/bin/env bash

# Enable curl
echo "extension=curl.so" > /opt/php-5.5/etc/conf.d/curl.ini
echo "extension=shmop.so" > /opt/php-5.5/etc/conf.d/shmop.ini

# Register webdevops ini
ln -sf "/opt/docker/etc/php/php.webdevops.ini" "${PHP_ETC_DIR}/98-webdevops.ini"

# Register custom php ini
ln -sf "/opt/docker/etc/php/php.ini" "${PHP_ETC_DIR}/99-docker.ini"
