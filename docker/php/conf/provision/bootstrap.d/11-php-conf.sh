#!/usr/bin/env bash

PHP_CLEAR_ENV_AVAILABLE=1
PHP_ETC_DIR=/opt/php-5.5
PHP_MOD_INI_DIR=/opt/php-5.5/etc/conf.d
PHP_MAIN_CONF=/opt/php-5.5/etc/php-fpm.conf
PHP_POOL_CONF=www.conf
PHP_POOL_DIR=/opt/php-5.5/etc/pool.d
PHP_FPM_BIN=/opt/php-5.5/sbin/php-fpm
